<?php

namespace App\Transformer;

use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

abstract class BaseTransformer
{
    protected $serializer;

    public function __construct()
    {
        $normalizers      = [
            new DateTimeNormalizer(),
            new ObjectNormalizer(
                null,
                new CamelCaseToSnakeCaseNameConverter(),
                null,
                new ReflectionExtractor()
            ),
        ];
        $encoders         = [
            new JsonEncoder(),
        ];
        $this->serializer = new Serializer($normalizers, $encoders);
    }
}