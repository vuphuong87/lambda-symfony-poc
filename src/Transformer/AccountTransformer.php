<?php

namespace App\Transformer;

use App\Message\AccountMessage;
use Exception;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class AccountTransformer extends BaseTransformer implements SerializerInterface
{
    /**
     * @param array $encodedEnvelope
     *
     * @return Envelope
     * @throws ExceptionInterface
     */
    public function decode(array $encodedEnvelope): Envelope
    {
        $body = $encodedEnvelope['body'];
        $data = json_decode($body, true);

        $message = $this->serializer->denormalize(
            $data,
            AccountMessage::class,
            'json'
        );

        return new Envelope($message);
    }

    /**
     * @param Envelope $envelope
     *
     * @return array
     * @throws Exception
     */
    public function encode(Envelope $envelope): array
    {
        throw new Exception('Transport & serializer not meant for sending messages');
    }
}