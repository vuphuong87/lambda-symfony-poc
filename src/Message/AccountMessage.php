<?php

namespace App\Message;

class AccountMessage
{
    /**
     * @var int
     */
    private $accountIdentifier;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string|null
     */
    private $nextgenIdentifier;

    /**
     * @return int
     */
    public function getAccountIdentifier(): int
    {
        return $this->accountIdentifier;
    }

    /**
     * @param int $accountIdentifier
     *
     * @return self
     */
    public function setAccountIdentifier(int $accountIdentifier): self
    {
        $this->accountIdentifier = $accountIdentifier;

        return $this;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNextgenIdentifier(): ?string
    {
        return $this->nextgenIdentifier;
    }

    /**
     * @param string|null $nextgenIdentifier
     *
     * @return self
     */
    public function setNextgenIdentifier(?string $nextgenIdentifier): self
    {
        $this->nextgenIdentifier = $nextgenIdentifier;

        return $this;
    }

}