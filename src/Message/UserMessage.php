<?php

namespace App\Message;

class UserMessage
{

    /**
     * @var string
     */
    private $userIdentifier;

    /**
     * @var int|null
     */
    private $accountIdentifier;

    /**
     * @return string
     */
    public function getUserIdentifier(): string
    {
        return $this->userIdentifier;
    }

    /**
     * @param string $userIdentifier
     *
     * @return self
     */
    public function setUserIdentifier(string $userIdentifier): self
    {
        $this->userIdentifier = $userIdentifier;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getAccountIdentifier(): ?int
    {
        return $this->accountIdentifier;
    }

    /**
     * @param int|null $accountIdentifier
     *
     * @return self
     */
    public function setAccountIdentifier(?int $accountIdentifier): self
    {
        $this->accountIdentifier = $accountIdentifier;

        return $this;
    }
}