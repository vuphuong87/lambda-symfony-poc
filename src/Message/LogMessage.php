<?php

namespace App\Message;

class LogMessage
{
    /**
     * @var string
     */
    private $action;

    /**
     * @var int
     */
    private $accountIdentifier;

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     *
     * @return self
     */
    public function setAction(string $action): self
    {
        $this->action = $action;

        return $this;
    }

    /**
     * @return int
     */
    public function getAccountIdentifier(): int
    {
        return $this->accountIdentifier;
    }

    /**
     * @param int $accountIdentifier
     *
     * @return self
     */
    public function setAccountIdentifier(int $accountIdentifier): self
    {
        $this->accountIdentifier = $accountIdentifier;

        return $this;
    }
}