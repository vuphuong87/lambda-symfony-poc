<?php

namespace App\Entity;

use App\Repository\AccountRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AccountRepository::class)
 */
class Account
{
    /**
     * @var int|null $accountId
     *
     * @ORM\Id
     * @ORM\Column(name="account_id", type="integer", unique=true, options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nextgenId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $vendor;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNextgenId(): ?string
    {
        return $this->nextgenId;
    }

    public function setNextgenId(string $nextgenId): self
    {
        $this->nextgenId = $nextgenId;

        return $this;
    }

    public function getVendor(): ?string
    {
        return $this->vendor;
    }

    public function setVendor(string $vendor): self
    {
        $this->vendor = $vendor;

        return $this;
    }
}
