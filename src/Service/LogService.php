<?php

namespace App\Service;

use App\Message\LogMessage;

class LogService
{
    /**
     * @param LogMessage $message
     */
    public function createLog(LogMessage $message)
    {
        echo $message->getAction() . PHP_EOL;
        echo $message->getAccountIdentifier() . PHP_EOL;
    }
}