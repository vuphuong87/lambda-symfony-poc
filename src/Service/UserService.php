<?php

namespace App\Service;

use App\Message\UserMessage;

class UserService
{
    /**
     * @param UserMessage $message
     */
    public function createLog(UserMessage $message)
    {
        echo $message->getAccountIdentifier() . PHP_EOL;
        echo $message->getUserIdentifier() . PHP_EOL;
    }
}