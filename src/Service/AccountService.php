<?php

namespace App\Service;

use App\Entity\Account;
use App\Message\AccountMessage;
use Doctrine\ORM\EntityManagerInterface;

class AccountService
{
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param AccountMessage $message
     */
    public function createLog(AccountMessage $message)
    {
        echo $message->getAccountIdentifier() . PHP_EOL;
        echo $message->getName() . PHP_EOL;
        echo $message->getNextgenIdentifier() . PHP_EOL;

        $account = $this->entityManager->getRepository(Account::class)->find($message->getAccountIdentifier());
        if ($account) {
            echo 'Account found: ' . PHP_EOL;
            echo $account->getName() . PHP_EOL;
        } else {
            echo 'Account not found: ' . PHP_EOL;
        }
    }
}