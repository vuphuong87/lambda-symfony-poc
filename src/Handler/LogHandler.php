<?php

namespace App\Handler;

use App\Message\LogMessage;
use App\Service\LogService;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class LogHandler implements MessageHandlerInterface
{
    protected $logService;

    public function __construct(LogService $logService)
    {
        $this->logService = $logService;
    }

    public function __invoke(LogMessage $message)
    {
        echo 'Start handling log message' . PHP_EOL;

        $this->logService->createLog($message);

        echo 'End handling log message' . PHP_EOL;
    }
}