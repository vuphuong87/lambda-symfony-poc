<?php

namespace App\Handler;

use App\Message\UserMessage;
use App\Service\UserService;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class UserHandler implements MessageHandlerInterface
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function __invoke(UserMessage $message)
    {
        echo 'Start handling user message' . PHP_EOL;

        $this->userService->createLog($message);

        echo 'End handling log message' . PHP_EOL;
    }
}