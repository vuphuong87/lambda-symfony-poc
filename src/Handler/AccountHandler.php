<?php

namespace App\Handler;

use App\Message\AccountMessage;
use App\Service\AccountService;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class AccountHandler implements MessageHandlerInterface
{
    protected $accountService;

    public function __construct(AccountService $accountService)
    {
        $this->accountService = $accountService;
    }

    public function __invoke(AccountMessage $message)
    {
        echo 'Start handling account message' . PHP_EOL;

        $this->accountService->createLog($message);

        echo 'End handling account message' . PHP_EOL;
    }
}